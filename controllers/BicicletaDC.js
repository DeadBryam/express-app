const bicicleta = require('../models/BicicletaDS');

exports.bicicleta_list = (req, res) => {
    bicicleta.find({}, (err, bicicletas) => {
        res.render('bicicletas/index', { bicis: bicicletas });
    });
}

exports.bicicleta_create_get = (req, res) => {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = (req, res) => {
    let bici = new bicicleta({ code: req.body.id, color: req.body.color, modelo: req.body.modelo });
    bici.ubicacion = [req.body.lat, req.body.lng];
    bicicleta.add(bici);

    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = (req, res) => {
    let bici = bicicleta.findById(req.params.id);

    res.render('bicicletas/update', { bici });
}

exports.bicicleta_update_post = (req, res) => {
    let bici = bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;

    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = (req, res) => {
    bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');
}