let Bicicleta = require('../../models/BicicletaDS');

exports.bicicleta_list = function(req, res){
  // res.status(200).json({
  //     bicicletas: Bicicleta.allBicis
  // });
  Bicicleta.find({}, (err, bicicletas) => {
    res.status(200).json({
      bicicletas: bicicletas
    });
  });
}

exports.bicicleta_create = function(req, res){
 
  let bici = new Bicicleta({code:req.body.id, color:req.body.color, modelo:req.body.modelo});
  bici.ubicacion = [req.body.lat, req.body.lng];

  Bicicleta.add(bici);

  res.status(200).json({
      bicicleta: bici
  });
}

exports.bicicleta_delete = function(req, res){
  Bicicleta.removeById(req.body.id);
  res.status(204).send();
}

exports.bicicleta_update = (req, res) => {
  let bici = Bicicleta.findById(req.params.id);
  bici.id = req.body.id;
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;

  res.status(200).json({
    bicicleta: bici
  });
}