var mymap = L.map('mapid').setView([13.858518,-89.80304], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(mymap);

$.ajax({
	dataType: "json",
	url: "api/bicicletas",
	success: (res)=>{
		console.log('sus ajax',res);
		res.bicicleta.forEach(el => {
			L.marker(el.ubicacion, {title: el.id}).addTo(mymap);		
		});
	}
});
