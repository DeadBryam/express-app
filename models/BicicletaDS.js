const mongoose = require('mongoose');
let schema = mongoose.Schema;

let bicicletaSchema = new schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function(code,color,modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function(){
    return `code: ${this.code} | color: ${this.color}`;
};

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({},cb);
};

bicicletaSchema.statics.add = function(entity,cb){
    console.log('paaso');
    console.log(entity);
    return this.create(entity,cb);
};

bicicletaSchema.statics.findByCode = function(code,cb){
    return this.findOne({code: code},cb);
}

bicicletaSchema.statics.removeByCode = function(code,cb){
    return this.deleteOne({code: code},cb);
}

module.exports = mongoose.model('Bicicleta',bicicletaSchema);