const mongoose = require('mongoose');
let Reserva = require('./ReservaDS');
const bcrypt = require('bcrypt');
const uniqeuValidator = require('mongoose-unique-validator');
const crypto = require('crypto');
const Token = require('../models/token');
const mailer = require('../mailer/mailer');

const saltRounds = 10;
let Schema = mongoose.Schema;

const validateEmail = (email) => {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

let usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, 'El nombre es obligatorio']
  },
  email: {
    type: String,
    trim: true,
    required: [true, 'El email es obligatorio'],
    lowercase: true,
    unique: true,
    validate: [validateEmail, 'Porfavor ingrese un Email valido'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
  },
  password: {
    type: String,
    required: [true, 'La contrasenia es obligatoria']
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false
  },
  googleId: String,
  facebookId: String
});

usuarioSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }

  next();
});

usuarioSchema.plugin(uniqeuValidator, { message: 'El {PATH} ya existe con otro usuario.' });

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function (id, desde, hasta, cb) {
  let reserva = new Reserva({ usuario: this._id, bicicleta: id, desde: desde, hasta: hasta })
  console.log('reserva', reserva);
  reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
  const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) { return console.log(err.message); }
    const mailOptions = {
      from: 'no-reply@redbicicletas.com',
      to: email_destination,
      subject: 'Verificacion de cuenta',
      text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' + process.env.HOST + '\/token/confirmation\/' + token.token + '.\n'
    };
    mailer.sendMail(mailOptions, function (err) {
      if (err) { return console.log("Error: ", err.message); }

      console.log('Se ha enviado un mail de bienvenida a: ' + email_destination + '.');
    });
  });

};

usuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return cb(err);
    }
    const mailOptions = {
      from: 'no-reply@redbicicletas.com',
      to: email_destination,
      subject: 'Reseteo de password de cuenta',
      text: 'Hola,\n\n' + 'Por favor para resetear el password de su cuenta haga click aqui: \n' +
        process.env.HOST + '\/resetPassword\/' + token.token + '.\n'
    };
    mailer.sendMail(mailOptions, function (err) {
      if (err) { return cb(err); }
      console.log('Se envio un email para resetear el password a: ' + email_destination + '.');
    });
    cb(null);
  });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
  const self = this;
  console.log(condition);
  self.findOne({
    $or: [
      { 'googleId': condition.id }, { 'email': condition.emails[0].value }
    ]
  }, (err, result) => {
    if (result) {
      callback(err, result)
    } else {
      console.log('-------Condition--------');
      console.log(condition);
      let values = {};
      values.googleId = condition.id;
      values.email = condition.emails[0].value;
      values.nombre = condition.displayName || 'Sin Nombre';
      values.verificado = true;
      values.password = condition._json.etag;
      console.log('-------Values------');
      console.log(values)
      self.create(values, (err, result) => {
        if (err) {
          console.log(err);
          console.log(err);
        }
        return callback(err, result)
      })
    }
  }
  )
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
  const self = this;
  console.log(condition);
  self.findOne({
    $or: [
      { 'facebookId': condition.id }, { 'email': condition.emails[0].value }
    ]
  }, (err, result) => {
    if (result) {
      callback(err, result)
    } else {
      console.log('-------Condition--------');
      console.log(condition);
      let values = {};
      values.facebookId = condition.id;
      values.email = condition.emails[0].value;
      values.nombre = condition.displayName || 'Sin Nombre';
      values.verificado = true;
      values.password = crypto.randomBytes(16).toString('hex');
      console.log('-------Values------');
      console.log(values)
      self.create(values, (err, result) => {
        if (err) console.log(err);
        return callback(err, result)
      })
    }
  }
  )
}

module.exports = mongoose.model('Usuario', usuarioSchema);