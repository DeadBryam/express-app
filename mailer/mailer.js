const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

// const mailConfig = {
//     host: 'smtp.ethereal.email',
//     port: 587,
//     secure: false, // true for 465, false for other ports
//     auth: {
//         user: 'ivah.labadie15@ethereal.email', // generated ethereal user velda.cormier@ethereal.email
//         pass: 'JCZq4zKxeukYJHkKRS' // generated ethereal password kbUrpQF8NmmZRqKsZT
//     }
// };

let mailConfig;
if (process.env.NODE_ENV === "production") {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
} else if (process.env.NODE_ENV === "staging") {
    console.log('XXXXXXXXXXXXXXXXXXXX');
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
} else {
    mailConfig = {
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false,
        auth: {
            user: process.env.ethereal_user,
            pass: process.env.ethereal_pwd
        }
    }
}


module.exports = nodemailer.createTransport(mailConfig);