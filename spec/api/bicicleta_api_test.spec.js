let bicicleta = require('../../models/BicicletaDS');
let request = require('request');
let server = require('../../bin/www');
let mongoose = require('mongoose');

const BASE_URL = 'http://127.0.0.1:3000/api/bicicletas';
const MONGO_URL = 'mongodb://127.0.0.1/testDB';


describe('Bicleta API', () => {
  beforeEach((done) => {
    mongoose.connect(MONGO_URL, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      console.log('We r connected.');
      done();
    });
  });

  afterEach((done) => {
    bicicleta.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
  });

  describe('GET BICICLETAS / ', () => {
    it('Status 200', (done) => {
      request.get(BASE_URL, (error, response, body) => {
        let result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe('POST BICICLETAS /create ', () => {
    it('Status 201', (done) => {
      let headers = { 'content-type': 'application/json' };
      let a = '{"id": 13, "color": "rojo", "modelo": "urbana", "lat": "-35", "lng": "40"}';

      request.post({
        headers: headers,
        url: `${BASE_URL}/create`,
        body: a
      }, (error, response, body) => {
        let result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicleta.color).toBe('rojo');
        expect(result.bicicleta.ubicacion[0]).toBe(-35);
        expect(result.bicicleta.ubicacion[1]).toBe(40);

        done();
      })

    });
  });

});

// describe('Bicicleta api', () => {
//   describe('GET BICICLETAS / ', () => {
//     it('Status 200', () => {
//       expect(bicicleta.allBicis.length).toBe(0);

//       let a = new bicicleta(1, 'verde', 'urbana', [13.858518, -89.803040]);
//       bicicleta.add(a);

//       request.get(BASE_URL, (error, response, body) => {
//         expect(response.statusCode).toBe(200);
//       })
//     });
//   });

//   describe('POST BICICLETAS /create ', () => {
//     it('Status 201', (done) => {
//       let headers = { 'content-type': 'application/json' };
//       let a = '{"id": 13, "color": "rojo", "modelo": "urbana", "lat": "-35", "long": "40"}';

//       request.post({
//         headers: headers,
//         url: `${BASE_URL}/create`,
//         body: a
//       }, (error, response, body) => {
//         expect(response.statusCode).toBe(201);
//         expect(bicicleta.findById(13).color).toBe('rojo');
//         done();
//       })

//     });
//   });

//   describe('PUT BICICLETAS /update ', () => {
//     let headers = { 'content-type': 'application/json' };
//     let a = new bicicleta(13, 'verde', 'urbana', [13.858518, -89.803040]);
//     let b = '{"id": 13, "color": "azul", "modelo": "montania", "lat": "-35", "long": "40"}';

//     it('status 200', (done) => {
//       // expect(bicicleta.allBicis.length).toBe(0);

//       // bicicleta.add(a);

//       // expect(bicicleta.allBicis.length).toBe(1);

//       request.put({
//         headers: headers,
//         url: `${BASE_URL}/13/update`,
//         body: b
//       }, (error, response, body) => {
//         expect(response.statusCode).toBe(200);
//         expect(bicicleta.findById(13).color).toBe('azul');
//         done();
//       });
//     });

//   });

//   describe('DELETE BICICLETAS /delete ', () => {
//     let headers = { 'content-type': 'application/json' };
//     let a = new bicicleta(13, 'verde', 'urbana', [13.858518, -89.803040]);
//     let b = '{"id": 13}';

//     it('Status 204', (done) => {
//       // expect(bicicleta.allBicis.length).toBe(0);

//       // bicicleta.add(a);

//       // expect(bicicleta.allBicis.length).toBe(1);

//       request.delete({
//         headers: headers,
//         url: `${BASE_URL}/delete`,
//         body: b
//       }, (error, response, body) => {
//         expect(response.statusCode).toBe(204);
//         done();
//       });
//     });
//   });

// });