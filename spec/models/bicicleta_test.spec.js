const mongoose = require('mongoose');
const MONGO_URL = 'mongodb://127.0.0.1/testDB';
let bicicleta = require('../../models/BicicletaDS');

describe('Testing bicicletas', () => {
  beforeEach((done) => {
    mongoose.connect(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      console.log('We r connected.');
      done();
    });
  });

  afterEach((done) => {
    bicicleta.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
  });

  describe('bicicleta.createInstace', () => {
    it('crea una instacia de bicicletas', () => {
      let instance = bicicleta.createInstance(1, 'verde', 'urbana', [-35, 40]);

      expect(instance.code).toEqual(1);
      expect(instance.color).toBe('verde');
      expect(instance.modelo).toBe('urbana');
      expect(instance.ubicacion[0]).toBe(-35);
      expect(instance.ubicacion[1]).toBe(40);
    });
  });

  describe('bicicleta.allBicis', () => {
    it('comienza vacia,', (done) => {
      bicicleta.allBicis((err, bicis) => {
        console.log(bicis);
        expect(bicis.length).toBe(0);
        done();
      });
    });

    describe('bicicleta.add', () => {
      it('agrega una bici', (done) => {
        let a = new bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });
        bicicleta.add(a, (err, newBici) => {
          if (err) console.log(err);
          bicicleta.allBicis((err, res) => {
            expect(res.length).toEqual(1);
            expect(res[0].code).toEqual(a.code);

            done();
          });
        });
      });
    });

    describe('bicicleta.findByCode', () => {
      it ('debe devolver la bici con code 1', (done) => {
        bicicleta.allBicis((error, res) => {
          expect(res.length).toBe(0);

          let a = new bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });
          let b = new bicicleta({ code: 2, color: 'azul', modelo: 'urbana' });

          bicicleta.add(a, (err, entity) => {
            if (err) console.log(err);

            bicicleta.add(b, () => {
              if (err) console.log(err);
              bicicleta.findByCode(2, (err, target) => {
                expect(target.code).toBe(b.code);
                expect(target.color).toBe(b.color);
                expect(target.modelo).toBe(b.modelo);

                done();
              });
            });
          });
        });
      });
    });

    describe('bicicleta.removeByCode', ()=>{
      it('debe eliminar la bici con el id 1', (done)=>{
        bicicleta.allBicis((err,res)=>{
          expect(res.length).toBe(0);

          let a = new bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });

          bicicleta.add(a, (err,entity) => {
            if (err) console.log(err);

            bicicleta.removeByCode(1,(err,target)=>{
              bicicleta.allBicis((err,target)=>{
                expect(target.length).toBe(0);

                done();
              });
            });
          });
        });
      });
    });

  });
});

// beforeEach(()=>{
//   bicicleta.allBicis = [];
// })

// describe('Bicicleta.allBicis', () => {
//   it('comienza vacio', () => {
//     expect(bicicleta.allBicis.length).toBe(0);
//   })
// });

// describe('Bicicleta.add', () => {
//   it('agregar bicicleta', () => {
//     expect(bicicleta.allBicis.length).toBe(0);

//     let a = new bicicleta(1, 'verde', 'urbana', [13.858518, -89.803040]);
//     bicicleta.add(a);

//     expect(bicicleta.allBicis.length).toBe(1);
//     expect(bicicleta.allBicis[0]).toBe(a);
//   })
// });

// describe('Bicicleta.findById', () => {
//   it('debe devolver la bici con id 1', () => {
//     expect(bicicleta.allBicis.length).toBe(0);

//     let a = new bicicleta(1, 'verde', 'urbana', [13.858518, -89.803040]);
//     let b = new bicicleta(2, 'azul', 'montania', [13.8568515, -89.7995854]);
//     let target;

//     bicicleta.add(a);
//     bicicleta.add(b);

//     target = bicicleta.findById(1);

//     expect(target.id).toBe(a.id);
//     expect(target.color).toBe(a.color);
//     expect(target.modelo).toBe(a.modelo);
//   })
// });

// describe('Bicicleta.removeById', () => {
//   it('debe eliminar la bici con id 1', () => {
//     expect(bicicleta.allBicis.length).toBe(0);

//     let a = new bicicleta(1, 'verde', 'urbana', [13.858518, -89.803040]);
//     bicicleta.add(a);

//     expect(bicicleta.allBicis.length).toBe(1);

//     bicicleta.removeById(1);

//     expect(bicicleta.allBicis.length).toBe(0);
//   })
// });

