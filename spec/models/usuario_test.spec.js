let mongoose = require('mongoose');
let Bicicleta = require('../../models/BicicletaDS');
let Usuario = require('../../models/UsuarioDS');
let Reserva = require('../../models/ReservaDS');
const MONGO_URL = 'mongodb://localhost/testDB';

describe('Testing Usuarios', function(){
    beforeEach(function(done){
        
        mongoose.connect(MONGO_URL, { userNewUrlParse: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connectedto test database');

            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva bici', () => {
        it('desde existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Juaquin'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: 'verde', modelo: 'urbana'})
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasReserva()).toBe(1);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
}) 