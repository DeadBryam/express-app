let express = require('express');
let router = express.Router();
let bicicletaDC = require('../../controllers/api/BicicletaRest');

router.get('/',bicicletaDC.bicicleta_list);
router.post('/create',bicicletaDC.bicicleta_create);
router.delete('/delete',bicicletaDC.bicicleta_delete);
router.put('/:id/update',bicicletaDC.bicicleta_update);


module.exports = router;