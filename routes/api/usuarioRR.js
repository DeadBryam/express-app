let express = require('express');
let router = express.Router();
let usuarioDC = require('../../controllers/api/UsuarioRest');

router.get('/',usuarioDC.usuario_list);
router.post('/create',usuarioDC.usuario_create);
router.post('/reservar',usuarioDC.usuario_reservar);

module.exports = router;