let express = require('express');
let router = express.Router();
let bicicletaDC = require('../controllers/BicicletaDC');

router.get('/',bicicletaDC.bicicleta_list);
router.get('/create',bicicletaDC.bicicleta_create_get);
router.post('/create',bicicletaDC.bicicleta_create_post);
router.get('/:id/update',bicicletaDC.bicicleta_update_get);
router.post('/:id/update',bicicletaDC.bicicleta_update_post);
router.post('/:id/delete',bicicletaDC.bicicleta_delete_post);

module.exports = router;